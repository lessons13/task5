package org.example.linkedlist;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.*;

public class SingleList<T> implements List<T> {
    private int length;
    private Node<T> head;
    private Node<T> tail;

    @Data
    @Accessors(chain = true)
    private static class Node<T>{
        private T value;
        private Node<T> next;
    }

    @Override
    public int size() {
        return length;
    }

    @Override
    public boolean isEmpty() {
        return head == null;
    }

    @Override
    public boolean contains(Object o) {
        for (T t : this) {
            if (t.equals(o)) return true;
        }
        return false;
    }

    @Override
    public Iterator<T> iterator() {
        return new Iterator<T>() {
            Node<T> cur = head;
            Node<T> prev = null;

            @Override
            public boolean hasNext() {
                return cur != null;
            }

            @Override
            public T next() {
                T val = cur.getValue();
                prev = cur;
                cur = cur.next;
                return val;
            }

            @Override
            public void remove() {
                removeNode(cur,prev);
                cur = cur.next;
            }
        };
    }

    @Override
    public Object[] toArray() {
        return toArray(new Object[0]);
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        a = Arrays.copyOf(a, length);
        int i = 0;
        for (T t : this) {
            a[i++] = (T1) t;
        }
        return a;
    }

    @Override
    public boolean add(T t) {
        if (head == null) {
            head = tail = new Node<T>().setValue(t);
        } else {
            var node = new Node<T>().setValue(t);
            tail.setNext(node);
            tail = node;
        }
        length++;
        return true;
    }

    private void removeNode(Node<T> cur, Node<T> prev) {
        if (cur == head && cur == tail) {
            head = tail = null;
            length = 0;
        } else {
            if (prev != null) prev.setNext(cur.next);

            if (cur == head) head = cur.next;
            else if (cur == tail) tail = prev;

            length--;
        }
    }

    @Override
    public boolean remove(Object o) {
        Node<T> prev = null;
        for (Node<T> cur = head; cur != null; cur = cur.next) {
            if (Objects.equals(cur.value, o)) {
                removeNode(cur,prev);
                return true;
            }
            prev = cur;
        }
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return c.stream().allMatch(this::contains);
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        c.forEach(this::add);
        return true;
    }

    @Override
    public boolean addAll(int index, Collection<? extends T> c) {
        if ((index < 0 || index > size())) throw new IndexOutOfBoundsException();
        if(c.isEmpty()) return false;

        SingleList<T> newItems = new SingleList<>();
        c.stream().forEach(newItems::add);
        if (length == 0) {
            head = newItems.head;
            tail = newItems.tail;
        } else if (index == 0) {
            newItems.tail.next = head;
            head = newItems.head;
        } else if (index == length) {
            tail.next = newItems.head;
            tail = newItems.tail;
        } else {
            Node<T> cur = head;
            Node<T> prev = null;
            for (int i = 0; i < index; i++) {
                prev = cur;
                cur = cur.next;
            }
            prev.next = newItems.head;
            newItems.tail.next = cur;
        }
        length = length + newItems.length;
        return true;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        boolean isModified = false;
        Node<T> prev = null;
        for (Node<T> cur = head; cur != null; cur = cur.next) {
            if (c.contains(cur.value)) {
                removeNode(cur, prev);
                isModified = true;
            }
            prev = cur;
        }
        return isModified;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        boolean isModified = false;
        Node<T> prev = null;
        for (Node<T> cur = head; cur != null; cur = cur.next) {
            if (!c.contains(cur.value)) {
                removeNode(cur, prev);
                isModified = true;
            }
            prev = cur;
        }
        return isModified;
    }

    @Override
    public void clear() {
        head = tail = null;
        length = 0;
    }

    @Override
    public T get(int index) {
        if (index < 0 || index >= length) throw new IndexOutOfBoundsException();
        Node<T> cur = head;
        for (int i = 0; i < index; i++) cur = cur.getNext();
        return cur.getValue();
    }

    @Override
    public T set(int index, T element) {
        if (index < 0 || index >= length) throw new IndexOutOfBoundsException();
        Node<T> cur = head;
        for (int i = 0; i < index; i++) cur = cur.getNext();
        cur.setValue(element);
        return element;
    }

    @Override
    public void add(int index, T element) {
        if ((index < 0 || index > size())) throw new IndexOutOfBoundsException();
        Node<T> newItem = new Node<>();
        newItem.setValue(element);

        if (length == 0) {
            head = newItem;
            tail = newItem;
        } else if (index == 0) {
            newItem.next = head;
            head = newItem;
        } else if (index == length) {
            tail.next = newItem;
            tail = newItem;
        } else {
            Node<T> cur = head;
            Node<T> prev = null;
            for (int i = 0; i < index; i++) {
                prev = cur;
                cur = cur.next;
            }
            prev.next = newItem;
            newItem.next = cur;
        }
        length = length + 1;
    }

    @Override
    public T remove(int index) {
        if ((index < 0 || index > size())) throw new IndexOutOfBoundsException();
        Node<T> cur = head;
        Node<T> prev = null;
        for (int i = 0; i < index; i++) {
            prev = cur;
            cur = cur.next;
        }
        removeNode(cur,prev);
        return cur.value;
    }

    @Override
    public int indexOf(Object o) {
        Node<T> cur = head;
        for (int i = 0; i < length; i++) {
            if (cur.value.equals(o)) {
                return i;
            }
            cur = cur.next;
        }
        return -1;
    }

    @Override
    public int lastIndexOf(Object o) {
        Node<T> cur = head;
        int lastIndex = -1;
        for (int i = 0; i < length; i++) {
            if (cur.value.equals(o)) {
                lastIndex = i;
            }
            cur = cur.next;
        }
        return lastIndex;
    }

    @Override
    public ListIterator<T> listIterator() {
        return null;
    }

    @Override
    public ListIterator<T> listIterator(int index) {
        return null;
    }

    @Override
    public List<T> subList(int fromIndex, int toIndex) {
        return null;
    }
}
