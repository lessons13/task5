package org.example.linkedlist;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class BiListTest {

    @Test
    @DisplayName("Добавление и получение по индексу")
    public void testAddAndGet() {
        List<Integer> list = new BiList<>();

        assertTrue(list.isEmpty());

        list.add(10);
        list.add(20);
        list.add(60);
        list.add(80);

        assertFalse(list.isEmpty());
        assertEquals(4, list.size());
        assertEquals(10, list.get(0));
        assertEquals(20, list.get(1));
        assertEquals(60, list.get(2));
        assertEquals(80, list.get(3));
    }

    @Test
    @DisplayName("Проверка итератора")
    public void testIterator() {
        List<String> list = new BiList<>();
        list.add("a");
        list.add("b");
        list.add("c");
        list.add("d");
        //String res = "";
        StringBuilder res = new StringBuilder();
        for (String s : list) {
            //res+=s;
            res.append(s);
        }
        //assertEquals("abcd", res);
        assertEquals("abcd", res.toString());
    }

    @Test
    @DisplayName("Преобразование в список")
    public void testToArray() {
        List<Integer> list = new BiList<>();

        list.add(10);
        list.add(20);
        list.add(60);
        list.add(80);

        Integer[] arr1 = list.toArray(new Integer[0]);
        assertArrayEquals(new Integer[]{10,20,60,80}, arr1);

        Object[] arr2 = list.toArray();
        assertArrayEquals(new Object[]{10,20,60,80}, arr2);
    }

    @Test
    @DisplayName("Удаление из списка")
    public void testRemove() {
        List<Integer> list = new BiList<>();

        list.add(10);
        list.add(20);
        list.add(60);
        list.add(80);
        list.add(100);

        //remove head
        Iterator<Integer> it0 = list.iterator();
        it0.remove();
        //list.iterator().remove();
        assertEquals(4, list.size());
        assertArrayEquals(new Integer[]{20,60,80,100}, list.toArray(new Integer[0]));

        //remove second element
        Iterator<Integer> it1 = list.iterator();
        it1.next();
        it1.remove();;
        assertEquals(3, list.size());
        assertArrayEquals(new Integer[]{20,80,100}, list.toArray(new Integer[0]));

        //remove tail
        Iterator<Integer> it2 = list.iterator();
        it2.next();
        it2.next();
        it2.remove();;
        assertEquals(2, list.size());
        assertArrayEquals(new Integer[]{20,80}, list.toArray(new Integer[0]));
    }

    @Test
    @DisplayName("Удаление по значению")
    public void testRemoveValue() {
        List<Integer> list = new BiList<>();

        list.add(10);
        list.add(20);
        list.add(60);
        list.add(80);
        list.add(100);

        assertTrue(list.remove(Integer.valueOf(80)));
        assertArrayEquals(new Integer[]{10,20,60,100}, list.toArray(new Integer[0]));

        assertFalse(list.remove(Integer.valueOf(90)));
        assertArrayEquals(new Integer[]{10,20,60,100}, list.toArray(new Integer[0]));

        assertTrue(list.remove(Integer.valueOf(10)));
        assertArrayEquals(new Integer[]{20,60,100}, list.toArray(new Integer[0]));

        assertTrue(list.remove(Integer.valueOf(100)));
        assertArrayEquals(new Integer[]{20,60}, list.toArray(new Integer[0]));
    }

    @Test
    public void testContains() {
        List<Integer> list = new BiList<>();

        assertTrue(list.isEmpty());

        list.add(10);
        list.add(20);
        list.add(60);
        list.add(80);
        list.add(100);

        assertFalse(list.contains(null));
        assertTrue(list.contains(10));
        assertTrue(list.contains(100));
        assertTrue(list.contains(60));
        assertFalse(list.contains(30));
    }

    @Test
    public void testContainsAll() {
        List<Integer> list = new BiList<>();

        assertTrue(list.isEmpty());

        list.add(10);
        list.add(20);
        list.add(60);
        list.add(80);
        list.add(100);

        assertFalse(list.contains(null));
        assertTrue(list.containsAll(Arrays.asList(10,20,60,80,100)));
        assertTrue(list.containsAll(Arrays.asList(20,100)));
        assertFalse(list.containsAll(Arrays.asList(20,30,100)));
        assertFalse(list.containsAll(Arrays.asList(10,20,50,80,100)));
    }

    @Test
    public void testAddAll() {
        List<Integer> list = new BiList<>();
        list.add(10);
        list.add(20);
        list.add(60);
        list.add(80);
        list.add(100);
        assertEquals(5, list.size());

        List<Integer> list1 = new BiList<>();
        list1.add(33);
        list1.add(55);

        list.addAll(2, list1);
        assertEquals(7, list.size());
        assertArrayEquals(new Integer[]{10,20,33,55,60,80,100}, list.toArray(new Integer[0]));

        list.addAll(0, list1);
        assertEquals(9, list.size());
        assertArrayEquals(new Integer[]{33,55,10,20,33,55,60,80,100}, list.toArray(new Integer[0]));

        list.addAll(9, list1);
        assertEquals(11, list.size());
        assertArrayEquals(new Integer[]{33,55,10,20,33,55,60,80,100,33,55}, list.toArray(new Integer[0]));

        List<Integer> list2 = new BiList<>();
        list2.addAll(0, list1);
        assertEquals(2, list2.size());
        assertArrayEquals(new Integer[]{33,55}, list2.toArray(new Integer[0]));

        List<Integer> list3 = new BiList<>();
        list.addAll(0, list3);
        assertEquals(11, list.size());
        assertArrayEquals(new Integer[]{33,55,10,20,33,55,60,80,100,33,55}, list.toArray(new Integer[0]));
    }

    @Test
    public void testRemoveAll() {
        List<Integer> list = new BiList<>();
        list.add(10);
        list.add(20);
        list.add(60);
        list.add(80);
        list.add(100);
        assertEquals(5, list.size());

        assertTrue(list.removeAll(Arrays.asList(10,60)));
        assertEquals(3, list.size());
        assertArrayEquals(new Integer[]{20,80,100}, list.toArray(new Integer[0]));

        assertTrue(list.removeAll(Arrays.asList(25,13,80)));
        assertEquals(2, list.size());
        assertArrayEquals(new Integer[]{20,100}, list.toArray(new Integer[0]));

        assertFalse(list.removeAll(Arrays.asList(22,11,66)));
        assertEquals(2, list.size());
        assertArrayEquals(new Integer[]{20,100}, list.toArray(new Integer[0]));
    }

    @Test
    public void testRetainAll() {
        List<Integer> list = new BiList<>();
        list.add(10);
        list.add(20);
        list.add(60);
        list.add(80);
        list.add(100);
        assertEquals(5, list.size());

        assertFalse(list.retainAll(Arrays.asList(10,20,60,80,100)));
        assertEquals(5, list.size());
        assertArrayEquals(new Integer[]{10,20,60,80,100}, list.toArray(new Integer[0]));

        assertTrue(list.retainAll(Arrays.asList(10,60,80,100)));
        assertEquals(4, list.size());
        assertArrayEquals(new Integer[]{10,60,80,100}, list.toArray(new Integer[0]));

        assertTrue(list.retainAll(Arrays.asList(10,60,77,100)));
        assertEquals(3, list.size());
        assertArrayEquals(new Integer[]{10,60,100}, list.toArray(new Integer[0]));

        assertTrue(list.retainAll(Arrays.asList(25,33,45,90)));
        assertEquals(0, list.size());
        assertArrayEquals(new Integer[]{}, list.toArray(new Integer[0]));
    }

    @Test
    public void testAdd() {
        List<Integer> list = new BiList<>();
        list.add(10);
        list.add(20);
        list.add(60);
        list.add(80);
        list.add(100);
        assertEquals(5, list.size());

        list.add(2, 25);
        assertEquals(6, list.size());
        assertArrayEquals(new Integer[]{10,20,25,60,80,100}, list.toArray(new Integer[0]));

        list.add(0, 5);
        assertEquals(7, list.size());
        assertArrayEquals(new Integer[]{5,10,20,25,60,80,100}, list.toArray(new Integer[0]));

        list.add(7, 111);
        assertEquals(8, list.size());
        assertArrayEquals(new Integer[]{5,10,20,25,60,80,100,111}, list.toArray(new Integer[0]));
    }

    @Test
    public void testRemoveIndex() {
        List<Integer> list = new BiList<>();
        list.add(10);
        list.add(20);
        list.add(60);
        list.add(80);
        list.add(100);
        assertEquals(5, list.size());

        assertEquals(80,list.remove(3));
        assertEquals(4, list.size());
        assertArrayEquals(new Integer[]{10,20,60,100}, list.toArray(new Integer[0]));

        assertEquals(10,list.remove(0));
        assertEquals(3, list.size());
        assertArrayEquals(new Integer[]{20,60,100}, list.toArray(new Integer[0]));

        assertEquals(100,list.remove(2));
        assertEquals(2, list.size());
        assertArrayEquals(new Integer[]{20,60}, list.toArray(new Integer[0]));
    }
    @Test
    public void testIndexOf() {
        List<Integer> list = new BiList<>();
        list.add(10);
        list.add(20);
        list.add(80);
        list.add(60);
        list.add(80);
        list.add(100);
        assertEquals(6, list.size());

        assertEquals(2,list.indexOf(80));
        assertEquals(-1,list.indexOf(120));
        assertEquals(0,list.indexOf(10));
        assertEquals(5,list.indexOf(100));
    }

    @Test
    public void testLastIndexOf() {
        List<Integer> list = new BiList<>();
        list.add(10);
        list.add(20);
        list.add(80);
        list.add(60);
        list.add(80);
        list.add(100);
        assertEquals(6, list.size());

        assertEquals(4,list.lastIndexOf(80));
        assertEquals(-1,list.lastIndexOf(120));
        assertEquals(0,list.lastIndexOf(10));
        assertEquals(5,list.lastIndexOf(100));
    }
}